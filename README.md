# Rabo Assignment

I used JHipster 6.5.1, you can find documentation and help at [https://www.jhipster.tech/documentation-archive/v6.5.1](https://www.jhipster.tech/documentation-archive/v6.5.1).
I created a basic domain model for my objects in the person-pet-jdl.jh file and then generated a basic crud app. I then wrote more code to do some of the more custom stuff. Improve the view, navigation and search.
Let me know what you guys think anyway. I would recommend just looking at the different commmits to see how specific things are done. I think they are relatively well named.

## Running

Before you can build this project, you must install and configure the following dependencies on your machine:

Install js dependencies

    npm install

Run the application

    ./mvnw

## Testing

To launch your application's tests, run:

    ./mvnw verify

## Client tests

Unit tests are run by [Jest][] and written with [Jasmine][]. They're located in [src/test/javascript/](src/test/javascript/) and can be run with:

    npm test

For more information, refer to the [Running tests page][].

## Using Docker

    docker-compose -f src/main/docker/mysql.yml up -d
