/**
 * View Models used by Spring MVC REST controllers.
 */
package com.code.nomads.web.rest.vm;
