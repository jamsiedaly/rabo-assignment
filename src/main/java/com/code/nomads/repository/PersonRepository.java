package com.code.nomads.repository;
import com.code.nomads.domain.Person;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


/**
 * Spring Data  repository for the Person entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PersonRepository extends JpaRepository<Person, Long> {

    @Query("SELECT p FROM Person p WHERE p.firstName = ?1 and p.lastName = ?2")
    Optional<Person> getOneByFirstNameAndLastName(String firstName, String lastName);

    @Query("SELECT p FROM Person p " +
        "WHERE UPPER(p.firstName) LIKE UPPER(CONCAT('%',?1,'%')) " +
        "or UPPER(p.lastName) LIKE UPPER(CONCAT('%',?1,'%'))")
    List<Person> searchPeople(String searchTerm);
}
