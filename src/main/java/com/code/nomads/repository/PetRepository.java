package com.code.nomads.repository;
import com.code.nomads.domain.Pet;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data  repository for the Pet entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PetRepository extends JpaRepository<Pet, Long> {

    @Query("SELECT p FROM Pet p WHERE UPPER(p.name) LIKE UPPER(CONCAT('%',?1,'%'))")
    List<Pet> searchPets(String searchTerm);
}
