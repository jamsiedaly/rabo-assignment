import { Moment } from 'moment';
import { IPerson } from 'app/shared/model/person.model';

export interface IPet {
  id?: number;
  name?: string;
  dateOfBirth?: Moment;
  person?: IPerson;
}

export class Pet implements IPet {
  constructor(public id?: number, public name?: string, public dateOfBirth?: Moment, public person?: IPerson) {}
}
