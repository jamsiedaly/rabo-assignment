import { Moment } from 'moment';
import { IPet } from 'app/shared/model/pet.model';

export interface IPerson {
  id?: number;
  firstName?: string;
  lastName?: string;
  dateOfBirth?: Moment;
  streetAddress?: string;
  postalCode?: string;
  city?: string;
  stateProvince?: string;
  pets?: IPet[];
}

export class Person implements IPerson {
  constructor(
    public id?: number,
    public firstName?: string,
    public lastName?: string,
    public dateOfBirth?: Moment,
    public streetAddress?: string,
    public postalCode?: string,
    public city?: string,
    public stateProvince?: string,
    public pets?: IPet[]
  ) {}
}
