import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IPerson } from 'app/shared/model/person.model';
import { PersonService } from './person.service';
import { PersonDeleteDialogComponent } from './person-delete-dialog.component';

@Component({
  selector: 'jhi-person',
  templateUrl: './person.component.html'
})
export class PersonComponent implements OnInit, OnDestroy {
  people: IPerson[];
  eventSubscriber: Subscription;
  currentSearch: string;

  constructor(
    protected personService: PersonService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected activatedRoute: ActivatedRoute
  ) {
    this.currentSearch =
      this.activatedRoute.snapshot && this.activatedRoute.snapshot.queryParams['search']
        ? this.activatedRoute.snapshot.queryParams['search']
        : '';
  }

  loadAll() {
    if (this.currentSearch) {
      this.personService.search(this.currentSearch).subscribe((res: HttpResponse<IPerson[]>) => (this.people = res.body));
    } else {
      this.personService.query().subscribe((res: HttpResponse<IPerson[]>) => {
        this.people = res.body;
        this.currentSearch = '';
      });
    }
  }

  ngOnInit() {
    this.loadAll();
    this.registerChangeInPeople();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: IPerson) {
    return item.id;
  }

  registerChangeInPeople() {
    this.eventSubscriber = this.eventManager.subscribe('personListModification', () => this.loadAll());
  }

  delete(person: IPerson) {
    const modalRef = this.modalService.open(PersonDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.person = person;
  }

  search(query) {
    if (!query) {
      return this.clear();
    }
    this.currentSearch = query;
    this.loadAll();
  }

  clear() {
    this.currentSearch = '';
    this.loadAll();
  }
}
