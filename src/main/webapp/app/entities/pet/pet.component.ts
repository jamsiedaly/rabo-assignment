import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IPet } from 'app/shared/model/pet.model';
import { PetService } from './pet.service';
import { PetDeleteDialogComponent } from './pet-delete-dialog.component';

@Component({
  selector: 'jhi-pet',
  templateUrl: './pet.component.html'
})
export class PetComponent implements OnInit, OnDestroy {
  pets: IPet[];
  eventSubscriber: Subscription;
  currentSearch: string;

  constructor(
    protected petService: PetService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected activatedRoute: ActivatedRoute
  ) {
    this.currentSearch =
      this.activatedRoute.snapshot && this.activatedRoute.snapshot.queryParams['search']
        ? this.activatedRoute.snapshot.queryParams['search']
        : '';
  }

  loadAll() {
    if (this.currentSearch) {
      this.petService.search(this.currentSearch).subscribe((res: HttpResponse<IPet[]>) => (this.pets = res.body));
    } else {
      this.petService.query().subscribe((res: HttpResponse<IPet[]>) => {
        this.pets = res.body;
        this.currentSearch = '';
      });
    }
  }

  ngOnInit() {
    this.loadAll();
    this.registerChangeInPets();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: IPet) {
    return item.id;
  }

  registerChangeInPets() {
    this.eventSubscriber = this.eventManager.subscribe('petListModification', () => this.loadAll());
  }

  delete(pet: IPet) {
    const modalRef = this.modalService.open(PetDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.pet = pet;
  }

  search(query) {
    if (!query) {
      return this.clear();
    }
    this.currentSearch = query;
    this.loadAll();
  }

  clear() {
    this.currentSearch = '';
    this.loadAll();
  }
}
