import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Pet } from 'app/shared/model/pet.model';
import { PetService } from './pet.service';
import { PetComponent } from './pet.component';
import { PetDetailComponent } from './pet-detail.component';
import { PetUpdateComponent } from './pet-update.component';
import { IPet } from 'app/shared/model/pet.model';

@Injectable({ providedIn: 'root' })
export class PetResolve implements Resolve<IPet> {
  constructor(private service: PetService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IPet> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(map((pet: HttpResponse<Pet>) => pet.body));
    }
    return of(new Pet());
  }
}

export const petRoute: Routes = [
  {
    path: '',
    component: PetComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Pets'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: PetDetailComponent,
    resolve: {
      pet: PetResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Pets'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: PetUpdateComponent,
    resolve: {
      pet: PetResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Pets'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: PetUpdateComponent,
    resolve: {
      pet: PetResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Pets'
    },
    canActivate: [UserRouteAccessService]
  }
];
