import { browser, ExpectedConditions as ec, protractor, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { PetComponentsPage, PetDeleteDialog, PetUpdatePage } from './pet.page-object';

const expect = chai.expect;

describe('Pet e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let petComponentsPage: PetComponentsPage;
  let petUpdatePage: PetUpdatePage;
  let petDeleteDialog: PetDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Pets', async () => {
    await navBarPage.goToEntity('pet');
    petComponentsPage = new PetComponentsPage();
    await browser.wait(ec.visibilityOf(petComponentsPage.title), 5000);
    expect(await petComponentsPage.getTitle()).to.eq('Pets');
  });

  it('should load create Pet page', async () => {
    await petComponentsPage.clickOnCreateButton();
    petUpdatePage = new PetUpdatePage();
    expect(await petUpdatePage.getPageTitle()).to.eq('Create or edit a Pet');
    await petUpdatePage.cancel();
  });

  it('should create and save Pets', async () => {
    const nbButtonsBeforeCreate = await petComponentsPage.countDeleteButtons();

    await petComponentsPage.clickOnCreateButton();
    await promise.all([
      petUpdatePage.setNameInput('name'),
      petUpdatePage.setDateOfBirthInput('01/01/2001' + protractor.Key.TAB + '02:30AM'),
      petUpdatePage.personSelectLastOption()
    ]);
    expect(await petUpdatePage.getNameInput()).to.eq('name', 'Expected Name value to be equals to name');
    expect(await petUpdatePage.getDateOfBirthInput()).to.contain(
      '2001-01-01T02:30',
      'Expected dateOfBirth value to be equals to 2000-12-31'
    );
    await petUpdatePage.save();
    expect(await petUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await petComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Pet', async () => {
    const nbButtonsBeforeDelete = await petComponentsPage.countDeleteButtons();
    await petComponentsPage.clickOnLastDeleteButton();

    petDeleteDialog = new PetDeleteDialog();
    expect(await petDeleteDialog.getDialogTitle()).to.eq('Are you sure you want to delete this Pet?');
    await petDeleteDialog.clickOnConfirmButton();

    expect(await petComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
